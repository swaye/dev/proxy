#!/usr/bin/env bash

set -e

echo 'CHECKING NETWORK: proxy_default'
if [[ $(docker network ls -f "name=proxy_default" --format '{{.Name}}') == 'proxy_default' ]]
then
    echo 'NETWORK FOUND!'
fi

echo 'CHECKING CONTAINER: proxy'
if [[ $(docker ps -f "name=proxy" --format '{{.Names}}') == 'proxy' ]]
then
    echo '!'
    docker stop proxy
    echo 'CONTAINER FOUND!'
    docker rm proxy
fi


echo 'PORTS: Scanning'
if nc 127.0.0.1 80 < /dev/null 2>/dev/null
then
    HTTP_PORT=8000
    HTTPS_PORT=8001
else
    HTTP_PORT=80
    HTTPS_PORT=443
fi
echo "PORTS: Using $HTTP_PORT, $HTTPS_PORT"

echo 'PROXY CONTAINER: Starting'
docker-compose up -d
echo 'PROXY CONTAINER: Started'
